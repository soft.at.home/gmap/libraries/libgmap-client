/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#if !defined(__GMAP_QUERY_H__)
#define __GMAP_QUERY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

typedef enum _gmap_query_action {
    gmap_query_expression_start_matching,   /* A device started matching the expression of the query */
    gmap_query_device_updated,              /* One or more parameters of a matching device have changed */
    gmap_query_expression_stop_matching,    /* A device stopped matching the expression of the query */
    gmap_query_error,                       /* An error occured for this query, this action can only occur on client side */
} gmap_query_action_t;

/**
   @brief
   Don't send out @ref gmap_query_device_updated events for matching devices.

   When this flag is used, a query may omit sending gmap_query_device_updated
   events when one or more parameters are changed for a matching device.

   It is highly encouraged to use this flag when your component doesn't rely on
   these events. gmap_query_device_updated events are expensive to send and receive
   and this flag can cut down significantly on the bus traffic generated
   by gmap-server.

   @warning
   Using this flag is a suggestion and not a guarantee that gmap_query_device_updated
   is not emitted. Clients may still receive these events, regardless of whether
   they used the flag or not. Client code must always be prepared to handle query
   events that it didn't request or doesn't recognize.

   Use function @ref gmap_query_flags_string to convert to a string representation.
   @see
   @ref GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR
 */
#define GMAP_QUERY_IGNORE_DEVICE_UPDATED        0x0001

/**
   @brief
   String representation of flag @ref GMAP_QUERY_IGNORE_DEVICE_UPDATED

   When this flag is used, a query may omit sending the usual gmap_query_device_updated
   events when one or more parameters are changed for a matching device.

   Use function @ref gmap_query_flags_from_cstring to convert to a bit mask.
 */
#define GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR    "ignore_device_updated"

typedef uint32_t gmap_query_flags_t;

/**
   @ingroup gmap_query
   @brief
   Converts a string containing flags into a bit field

   The string must contain device flags, separated with a '|' (pipe) sign.

   The following string are converted into a bit field:
   - @ref "ignore_device_updated" - GMAP_QUERY_IGNORE_DEVICE_UPDATED

   Any other value is not converted into a bit field

   @param flags_string The string containing the flags

   @return
   Returns the corresponding bit field
 */
gmap_query_flags_t gmap_query_flags_from_cstring(const char* flags);

/**
   @ingroup gmap_query
   @brief
   Converts flags into a string

   The following flags are converted into a string:
   - @ref GMAP_QUERY_IGNORE_DEVICE_UPDATED - "ignore_device_updated"

   All flags are separated with a '|' (pipe) sign.

   Any other value is not added to the string.

   @note
   - The returned pointer must be freed

   @param flags The bit field containing the flags

   @return
   Returns the corresponding string,
   or the empty string if no valid flags could be converted.
 */
amxc_string_t* gmap_query_flags_string(gmap_query_flags_t flags);

typedef struct _gmap_query_t gmap_query_t;

typedef void (* gmap_query_cb_t) (gmap_query_t* query, const char* key, amxc_var_t* device, gmap_query_action_t action);

/**
   @ingroup gmap_query
   @brief
   Internal query structure.

   @important
   The structure definition is considered an implementation detail and is only
   provided to simplify writing unit tests. Production code must use the provided
   accessor functions, such as @ref gmap_query_index and @ref gmap_query_name.

   @important
   The only member that may be freely read or written to, is the data pointer.
 */
struct _gmap_query_t {
    amxc_llist_it_t it;
    const char* expression;
    const char* name;
    gmap_query_flags_t flags;
    uint32_t id;
    uint32_t index;
    gmap_query_cb_t fn;
    uint32_t refcount;
    /**
       @brief
       Private client data pointer.

       This member may be used to store additional data with the query. This pointer
       is never accessed or written to by the library implementation.

       @see gmap_query_open_ext and gmap_query_open_ext_2
     */
    void* data;
};

/**
   @ingroup gmap_query
   @brief
   Opens a new gmap query.

   Calling this function is the same as calling directly:
   @code
   gmap_query_t* my_query = gmap_query_open_ext(expression, name, fn, NULL);
   @endcode

   @note
   - The opened query must be closed
   - Closing the query from the provided callback is supported.
   - Consider using @ref gmap_query_open_ext_2 if you are not interested in all
     query events.

   @param expression Query expression to match devices
   @param name A name for the query
   @param fn Callback function that will be called on matching device actions.

   @return a pointer to the newly created query object,
           or NULL on error,
           or NULL on success when the callback immediately closed the query.

   @see gmap_query_close
 */
gmap_query_t* gmap_query_open(const char* expression, const char* name, gmap_query_cb_t fn);

/**
   @ingroup gmap_query
   @brief
   Opens a new gmap query.

   Calling this function is the same as calling directly:
   @code
   gmap_query_t* my_query = NULL;
   gmap_query_open_ext_2(&my_query, expression, name, 0, fn, user_data);
   @endcode

   @note
   - The opened query must be closed
   - Closing the query from the provided callback is supported.
   - Consider using @ref gmap_query_open_ext_2 if you are not interested in all
     query events.

   @param expression Query expression to match devices
   @param name A name for the query
   @param fn Callback function that will be called on matching device actions.
   @param user_data User data that is stored untouched in the created query object.
                    The callback function can access this data as `query->data`.

   @return a pointer to the newly created query object,
           or NULL on error,
           or NULL on success when the callback immediately closed the query.

   @see gmap_query_close
 */
gmap_query_t* gmap_query_open_ext(const char* expression, const char* name, gmap_query_cb_t fn, void* user_data);
/**
   @ingroup gmap_query
   @brief
   Opens a new gmap query.

   @note
   - The opened query must be closed
   - Closing the query from the provided callback is supported.

   @param query Pointer to where a pointer to the created query object will be stored.
                If the query is closed by the callback before this function returns,
                NULL will be stored here and the operation is still considered
                successful.
   @param expression Query expression to match devices
   @param name A name for the query
   @param fn Callback function that will be called on matching device actions.
   @param flags A combination of flags to influence the event behavior.
                Valid flags are:
                - @ref GMAP_QUERY_IGNORE_DEVICE_UPDATED: Don't send out
                  @ref gmap_query_device_updated events for matching devices.
   @param user_data User data that is stored untouched in the created query object.
                    The callback function can access this data as `query->data`.

   @retval true if the query was opened successfully.
   @retval false if opening the query failed for some reason.

   @see gmap_query_close
 */
bool gmap_query_open_ext_2(gmap_query_t** query,
                           const char* expression,
                           const char* name,
                           gmap_query_flags_t flags,
                           gmap_query_cb_t fn,
                           void* user_data);

void gmap_query_close(gmap_query_t* query);

amxc_var_t* gmap_query_matchingDevices(gmap_query_t* query);

bool gmap_query_index(const gmap_query_t* gmap_query, uint32_t* index);
bool gmap_query_id(const gmap_query_t* gmap_query, uint32_t* id);
bool gmap_query_flags(const gmap_query_t* gmap_query, gmap_query_flags_t* flags);

const char* gmap_query_name(const gmap_query_t* gmap_query);
const char* gmap_query_expression(const gmap_query_t* gmap_query);

#ifdef __cplusplus
}
#endif

#endif // __GMAP_QUERY_H__
