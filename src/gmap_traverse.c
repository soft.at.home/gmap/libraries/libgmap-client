/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <gmap/gmap_traverse.h>
#include "gmap_priv.h"
#include "string.h"


static const char* gmap_traverse_mode_strs[] = {
    GMAP_TRAVERSE_THIS_STR,
    GMAP_TRAVERSE_DOWN_STR,
    GMAP_TRAVERSE_UP_STR,
    GMAP_TRAVERSE_DOWN_EXCLUSIVE_STR,
    GMAP_TRAVERSE_UP_EXCLUSIVE_STR,
    GMAP_TRAVERSE_ONE_DOWN_STR,
    GMAP_TRAVERSE_ONE_UP_STR,
    GMAP_TRAVERSE_ONE_DOWN_EXCLUSIVE_STR,
    GMAP_TRAVERSE_ONE_UP_EXCLUSIVE_STR,
};


/**
   @ingroup gmap_traverse
   @brief
   Converts a string into a traverse mode

   The following strings are converted into a traverse mode:
   - "this" - @ref gmap_traverse_this
   - "down" - @ref gmap_traverse_down
   - "up" - @ref gmap_traverse_up
   - "down exclusive" - @ref gmap_traverse_down_exclusive
   - "up exclusive" - @ref gmap_traverse_up_exclusive
   - "one level down" - @ref gmap_traverse_one_down
   - "one level up" - @ref gmap_traverse_one_up
   - "one level down exclusive" - @ref gmap_traverse_one_down_exclusive
   - "one level up exclusive" - @ref gmap_traverse_one_up_exclusive

   Any other string is converted in the @ref gmap_traverse_this traverse mode

   @param str the string that must be converted into a traverse mode

   @return
   Returns the corresponding traverse mode
 */
gmap_traverse_mode_t gmap_traverse_mode(const char* str) {
    gmap_traverse_mode_t m;
    if(!str) {
        m = gmap_traverse_this;
        goto exit;
    }

    for(m = gmap_traverse_this; m < gmap_traverse_max; m++) {
        if(strcmp(str, gmap_traverse_mode_strs[m]) == 0) {
            break;
        }
    }

exit:
    return (m >= gmap_traverse_max) ? gmap_traverse_this : m;
}

/**
   @ingroup gmap_traverse
   @brief
   Converts a traverse mode into a string

   The following traverse modes are converted into a string:
   - @ref gmap_traverse_this - "this"
   - @ref gmap_traverse_down - "down"
   - @ref gmap_traverse_up - "up"
   - @ref gmap_traverse_down_exclusive - "down exclusive"
   - @ref gmap_traverse_up_exclusive - "up exclusive"
   - @ref gmap_traverse_one_down - "one level down"
   - @ref gmap_traverse_one_up - "one level up"
   - @ref gmap_traverse_one_down_exclusive - "one level down exclusive"
   - @ref gmap_traverse_one_up_exclusive - "one level up exclusive"

   Any other value is converted into the string "this"

   @note
   - Do not free the returned pointer

   @param mode The mode that needs to be converted into a string

   @return
   Returns the corresponding string
 */
const char* gmap_traverse_string(gmap_traverse_mode_t mode) {
    if(mode < gmap_traverse_max) {
        return gmap_traverse_mode_strs[mode];
    }

    return gmap_traverse_mode_strs[gmap_traverse_this];
}
