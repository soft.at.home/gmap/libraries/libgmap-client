/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <gmap/gmap_query.h>
#include <gmap/gmap_main.h>
#include <gmap/gmap_traverse.h>
#include <gmap/gmap_devices_flags.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "gmap_priv.h"
#include <amxp/amxp_expression.h>
#include <amxd/amxd_object_expression.h>

#define ME "libgmap"

typedef struct _gmap_client_side_query {
    amxc_llist_it_t it;                 /**< Iterator used to store query in list */
    char* expression;                   /**< The expression used for the query */
    char* name;                         /**< Name of the query if provided */
    uint32_t index;                     /**< Index of the server-side query object */
    amxc_llist_t queries;               /**< All open queries for this expression */
    amxc_llist_t matching_devices;      /**< List of all matching devices */
    amxb_subscription_t* subscription;  /**< Event subscription */
} gmap_client_side_query_t;


gmap_query_flags_t gmap_query_flags_from_cstring(const char* flags) {
    gmap_query_flags_t ret = 0;
    amxc_llist_t flags_list;
    amxc_string_t flags_string;

    amxc_llist_init(&flags_list);
    amxc_string_init(&flags_string, 0);

    when_str_empty(flags, exit);

    amxc_string_appendf(&flags_string, "%s", flags);
    amxc_string_split_to_llist(&flags_string, &flags_list, '|');

    amxc_llist_for_each(it, &flags_list) {
        amxc_string_t* flag_string = amxc_string_from_llist_it(it);
        amxc_string_trim(flag_string, NULL);
        if(strcmp(amxc_string_get(flag_string, 0), GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR) == 0) {
            ret |= GMAP_QUERY_IGNORE_DEVICE_UPDATED;
            continue;
        }
    }

exit:
    amxc_llist_clean(&flags_list, amxc_string_list_it_free);
    amxc_string_clean(&flags_string);
    return ret;
}


amxc_string_t* gmap_query_flags_string(gmap_query_flags_t flags) {
    amxc_string_t* gmap_flags;
    amxc_string_new(&gmap_flags, flags ? 64 : 0);

    if(flags & GMAP_QUERY_IGNORE_DEVICE_UPDATED) {
        amxc_string_append(gmap_flags, GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR, strlen(GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR));
    }

    return gmap_flags;
}


bool gmap_query_id(const gmap_query_t* gmap_query, uint32_t* id) {
    bool ret = false;
    when_null_trace(gmap_query, exit, ERROR, "query is NULL pointer");
    when_null_trace(id, exit, ERROR, "id is NULL pointer");
    *id = gmap_query->id;
    ret = true;
exit:
    return ret;
}

bool gmap_query_index(const gmap_query_t* gmap_query, uint32_t* index) {
    bool ret = false;
    when_null_trace(gmap_query, exit, ERROR, "query is NULL pointer");
    when_null_trace(index, exit, ERROR, "index is NULL pointer");
    *index = gmap_query->index;
    ret = true;
exit:
    return ret;
}

bool gmap_query_flags(const gmap_query_t* gmap_query, gmap_query_flags_t* flags) {
    bool ret = false;
    when_null_trace(gmap_query, exit, ERROR, "query is NULL pointer");
    when_null_trace(flags, exit, ERROR, "flags is NULL pointer");
    *flags = gmap_query->flags;
    ret = true;
exit:
    return ret;
}

const char* gmap_query_name(const gmap_query_t* gmap_query) {
    const char* name = NULL;
    when_null_trace(gmap_query, exit, ERROR, "query is NULL pointer");
    gmap_client_side_query_t* collection = amxc_container_of(gmap_query->it.llist, gmap_client_side_query_t, queries);
    when_null(collection, exit);
    name = collection->name;
exit:
    return name;
}

const char* gmap_query_expression(const gmap_query_t* gmap_query) {
    const char* expression = NULL;
    when_null_trace(gmap_query, exit, ERROR, "query is NULL pointer");
    gmap_client_side_query_t* collection = amxc_container_of(gmap_query->it.llist, gmap_client_side_query_t, queries);
    when_null(collection, exit);
    expression = collection->expression;
exit:
    return expression;
}

static gmap_client_side_query_t* gmap_query_find_expression(const char* expression) {
    gmap_client_side_query_t* collection = NULL;
    when_null(expression, exit);

    amxc_llist_for_each(it, gmap_get_client_queries()) {
        collection = amxc_container_of(it, gmap_client_side_query_t, it);
        if(strcmp(collection->expression, expression) == 0) {
            SAH_TRACEZ_INFO(ME, "expression found");
            break;
        }
        collection = NULL;
    }
exit:
    return collection;
}

static amxc_string_t* gmap_query_get_matching_device(amxc_llist_t* devices, const char* key) {
    when_null(devices, exit);
    when_null(key, exit);
    amxc_llist_for_each(it, devices) {
        amxc_string_t* device = amxc_string_from_llist_it(it);
        if(strcmp(amxc_string_get(device, 0), key) == 0) {
            SAH_TRACEZ_INFO(ME, "device found");
            return device;
        }
    }
exit:
    return NULL;
}

static void gmap_query_execute(gmap_client_side_query_t* collection, const char* key, amxc_var_t* device, gmap_query_action_t action, gmap_query_cb_t fn, gmap_query_t* query) {
    when_null(collection, exit);
    when_null(key, exit);
    SAH_TRACEZ_INFO(ME, "gmap query execute");
    amxc_string_t* matching_device = gmap_query_get_matching_device(
        &collection->matching_devices,
        key);
    if(matching_device) {
        if(action == gmap_query_expression_start_matching) {
            if(fn) {
                /* called from open query:
                   device matching before, only call provided fn for query
                   (new for the query but already known by the collection) */
                SAH_TRACEZ_INFO(ME, "device: %s new match", key);
                fn(query, key, device, action);
            }
            return;
        }
    } else {
        if(action == gmap_query_expression_stop_matching) {
            SAH_TRACEZ_INFO(ME, "device removed, never matched, ignore");
            return;
        }
    }
    if(action == gmap_query_expression_start_matching) {
        SAH_TRACEZ_INFO(ME, "start matching, no yet in the collection, add device %s to collection", key);
        amxc_string_t* newdevice = NULL;
        amxc_string_new(&newdevice, 0);
        amxc_string_set(newdevice, key);
        amxc_llist_append(&collection->matching_devices, &newdevice->it);
    } else {
        if(action == gmap_query_expression_stop_matching) {
            SAH_TRACEZ_INFO(ME, "stop matching, already in the collection. remove device %s from collection", key);
            amxc_string_delete(&matching_device);
        }
    }
    /* call all open queries */
    amxc_llist_for_each(it, &collection->queries) {
        gmap_query_t* gmap_query = amxc_container_of(it, gmap_query_t, it);
        SAH_TRACEZ_INFO(ME, "call callback for query index:%d id:%d key: %s device: %p action: %d", gmap_query->index, gmap_query->id, key, device, action);
        gmap_query->fn(gmap_query, key, device, action);
    }
exit:
    return;
}

static void gmap_query_update_reply_handler(UNUSED const char* const sig_name,
                                            const amxc_var_t* const data,
                                            void* const priv) {

    amxc_var_t* result = NULL;
    amxc_var_t* device = NULL;
    gmap_query_action_t action;
    const char* key = NULL;

    SAH_TRACEZ_INFO(ME, "query update reply handler called");
    when_null_trace(data, exit, ERROR, "data is NULL pointer");

    gmap_client_side_query_t* collection = (gmap_client_side_query_t*) priv;
    when_null_trace(collection, exit, ERROR, "collection is NULL pointer");

    result = GET_ARG(data, "Result");
    when_null_trace(result, exit, ERROR, "result is NULL pointer");

    action = GET_UINT32(result, "Action");
    key = GET_CHAR(result, "Key");
    when_str_empty_trace(key, exit, ERROR, "key is empty string");
    device = GET_ARG(result, "Device");

    gmap_query_execute(collection, key, device, action, NULL, NULL);

exit:
    return;
}

static int gmap_query_subscribe_update(gmap_client_side_query_t* collection) {
    int ret = -1;
    amxc_string_t object;
    const char* expr = "notification == 'gmap_query'";

    when_null(collection, exit);

    SAH_TRACEZ_INFO(ME, "query subscribe update");

    amxc_string_init(&object, 0);
    amxc_string_setf(&object, "Devices.Query.%u.", collection->index);

    SAH_TRACEZ_INFO(ME, "query: %s, expr: %s\n", amxc_string_get(&object, 0), expr);

    ret = amxb_subscription_new(&collection->subscription,
                                gmap_get_bus_ctx(),
                                amxc_string_get(&object, 0),
                                expr,
                                gmap_query_update_reply_handler,
                                collection);
    when_failed_trace(ret, exit, ERROR, "Call gmap_query (query index: %d)  event subscription failed", collection->index);

exit:
    amxc_string_clean(&object);
    return ret;
}

static void gmap_query_unsubscribe_update(gmap_client_side_query_t* collection) {
    int ret = -1;
    when_null(collection, exit);
    SAH_TRACEZ_INFO(ME, "query unsubscribe update");
    ret = amxb_subscription_delete(&collection->subscription);
    if(ret) {
        SAH_TRACEZ_NOTICE(ME, "Call gmap_query event unsubscribe failed");
    }
exit:
    return;
}

static gmap_client_side_query_t* gmap_query_create_collection(const char* expression, const char* name, uint32_t index) {
    gmap_client_side_query_t* collection = NULL;
    when_null(expression, exit);
    when_null(name, exit);

    SAH_TRACEZ_INFO(ME, "create collection");
    collection = calloc(1, sizeof(gmap_client_side_query_t));
    when_null_trace(collection, exit, ERROR, "memory allocation failure");
    collection->expression = strdup(expression);
    when_null_trace(collection->expression, error, ERROR, "memory allocation failure");

    collection->name = strdup(name);
    when_null_trace(collection->name, error, ERROR, "memory allocation failure");
    collection->index = index;

    int ret = gmap_query_subscribe_update(collection);
    when_failed(ret, error);
    amxc_llist_append(gmap_get_client_queries(), &collection->it);
    SAH_TRACEZ_INFO(ME, "collection succesfully created");
exit:
    return collection;
error:
    free(collection->expression);
    free(collection->name);
    free(collection);
    collection = NULL;
    return collection;
}

static gmap_query_t* gmap_query_open_internal(const char* expression, uint32_t index, uint32_t id, const char* name, gmap_query_cb_t fn, gmap_query_flags_t flags) {

    gmap_query_t* gmap_query = NULL;
    gmap_client_side_query_t* query_collection = NULL;

    when_str_empty(expression, exit);
    when_str_empty(name, exit);
    when_null(fn, exit);

    SAH_TRACEZ_INFO(ME, "enter query open internal");

    gmap_query = calloc(1, sizeof(gmap_query_t));
    when_null_trace(gmap_query, exit, ERROR, "memory allocation failure");

    query_collection = gmap_query_find_expression(expression);

    if(!query_collection) {
        SAH_TRACEZ_INFO(ME, "query collection created");
        query_collection = gmap_query_create_collection(expression, name, index);
    }
    if(!query_collection) {
        free(gmap_query);
        gmap_query = NULL;
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "query collection found or created");
    gmap_query->fn = fn;
    gmap_query->id = id;
    gmap_query->index = index;
    gmap_query->flags = flags;
    SAH_TRACEZ_INFO(ME, "add query to collection");
    amxc_llist_append(&query_collection->queries, &gmap_query->it);
exit:
    return gmap_query;
}

gmap_query_t* gmap_query_open(const char* expression, const char* name, gmap_query_cb_t fn) {
    return gmap_query_open_ext(expression, name, fn, NULL);
}

gmap_query_t* gmap_query_open_ext(const char* expression, const char* name, gmap_query_cb_t fn, void* user_data) {
    gmap_query_t* query = NULL;
    gmap_query_open_ext_2(&query, expression, name, 0, fn, user_data);
    return query;
}

bool gmap_query_open_ext_2(gmap_query_t** query,
                           const char* expression,
                           const char* name,
                           gmap_query_flags_t flags,
                           gmap_query_cb_t fn,
                           void* user_data) {
    gmap_client_side_query_t* collection = NULL;
    int status = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* content;
    amxc_string_t* flags_str = NULL;
    const amxc_var_t* devices;
    uint32_t id;
    uint32_t index;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    SAH_TRACEZ_INFO(ME, "gmap query open: expression: %s name: %s fn: %p data: %p", expression, name, fn, user_data);
    when_null_trace(query, exit, ERROR, "query is NULL");
    *query = NULL;
    when_str_empty_trace(name, exit, ERROR, "name is empty string");
    when_str_empty_trace(expression, exit, ERROR, "expression is empty string");
    when_null(fn, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "expression", expression);
    amxc_var_add_key(cstring_t, &args, "name", name);
    if(flags != 0) {
        flags_str = gmap_query_flags_string(flags);
        amxc_var_push(amxc_string_t, amxc_var_add_new_key(&args, "flags"), flags_str);
    }

    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "openQuery",
                       &args,
                       &ret,
                       gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Call openQuery failed for expression: %s", expression);
    content = GETI_ARG(&ret, 0);
    status = -1;
    when_null_trace(content, exit, ERROR, "no output parameters returned from openquery");
    index = GET_UINT32(content, "index");
    id = GET_UINT32(content, "id");
    name = GET_CHAR(content, "name");
    when_str_empty_trace(name, exit, ERROR, "empty name returned from openquery");
    flags = gmap_query_flags_from_cstring(GET_CHAR(content, "flags"));
    SAH_TRACEZ_INFO(ME, "id:%d index:%d name: %s expression: %s", id, index, name, expression);
    *query = gmap_query_open_internal(expression, index, id, name, fn, flags);
    when_null(*query, exit);

    (*query)->data = user_data;
    (*query)->refcount = 1;

    SAH_TRACEZ_INFO(ME, "query: id:%d  index: %d  fn: %p", (*query)->id, (*query)->index, (*query)->fn);
    status = 0;

    // get devices

    devices = GET_ARG(content, "devices");
    when_null(devices, exit);

    /* collection list */
    collection = amxc_container_of((*query)->it.llist, gmap_client_side_query_t, queries);
    when_null(collection, exit);

    amxc_var_for_each(device, devices) {
        const char* key = GET_CHAR(device, "Key");
        SAH_TRACEZ_INFO(ME, "matching device returned: %s", key);
        gmap_query_execute(collection, key, device, gmap_query_expression_start_matching, fn, *query);
        if((*query)->refcount == 0) {
            /* The query was closed while executing the callback and the pointer
             * still needs to be deleted */
            free(*query);
            *query = NULL;
            goto exit;
        }
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_delete(&flags_str);
    if((query != NULL) && (*query != NULL)) {
        (*query)->refcount = 0;
    }
    return status == 0;
}

void gmap_query_close(gmap_query_t* gmap_query) {

    amxc_var_t args;
    amxc_var_t ret;
    int status = -1;
    gmap_client_side_query_t* query_collection = NULL;

    when_null_trace(gmap_query, exit, ERROR, "gmap_query is NULL pointer");
    SAH_TRACEZ_INFO(ME, "gmap_query_close");
    if(gmap_query->it.llist) {
        query_collection = amxc_container_of(gmap_query->it.llist,
                                             gmap_client_side_query_t,
                                             queries);
    }
    amxc_llist_it_take(&gmap_query->it);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "index", gmap_query->index);
    amxc_var_add_key(uint32_t, &args, "id", gmap_query->id);
    status = amxb_call(gmap_get_bus_ctx(),
                       "Devices",
                       "closeQuery",
                       &args,
                       &ret,
                       gmap_get_timeout());
    if(status) {
        SAH_TRACEZ_NOTICE(ME, "Call closequery failed");
    }
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    when_null(query_collection, exit);
    if(amxc_llist_is_empty(&query_collection->queries)) {
        SAH_TRACEZ_INFO(ME, "query collection empty index: %d id: %d", gmap_query->index, gmap_query->id);
        gmap_query_unsubscribe_update(query_collection);

        amxc_llist_it_take(&query_collection->it);
        free(query_collection->expression);
        free(query_collection->name);
        amxc_llist_clean(&query_collection->matching_devices, amxc_string_list_it_free);
        free(query_collection);
    }

exit:
    if((gmap_query != NULL) && (gmap_query->refcount > 0)) {
        /* This query is being closed from the callback while it is being opened.
         * Drop the refcount marker here to signal to gmap_query_open that it should
         * clean up the now empty query struct.
         */
        gmap_query->refcount = 0;
    } else {
        free(gmap_query);
    }
    return;
}

amxc_var_t* gmap_query_matchingDevices(gmap_query_t* query) {
    amxc_var_t* retv = NULL;
    amxc_string_t objname;

    int status = 0;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* content = NULL;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    when_null_trace(query, exit, ERROR, "query is NULL pointer");

    amxc_string_init(&objname, 0);
    amxc_string_setf(&objname, "Devices.Query.%s.", gmap_query_name(query));
    status = amxb_call(gmap_get_bus_ctx(),
                       amxc_string_get(&objname, 0),
                       "matchingDevices",
                       &args,
                       &ret,
                       gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Call matching devices failed for query name %s", query->name);
    content = GETI_ARG(&ret, 0);
    when_null_trace(content, exit, ERROR, "no output parameters returned from openquery");

    amxc_var_new(&retv);
    amxc_var_move(retv, content);
exit:
    amxc_string_clean(&objname);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retv;
}
