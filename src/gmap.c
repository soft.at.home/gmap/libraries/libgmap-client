/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <gmap/gmap_main.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include "debug/sahtrace_macros.h"
#include "gmap_priv.h"

#define ME "libgmap"

typedef struct _gmap_client {
    amxb_bus_ctx_t* bus_ctx;
    amxc_llist_t queries;
    amxc_llist_t event_handlers;
    amxc_llist_t config_event_handlers;
    amxc_llist_t device_functions;
    bool gmap_event_subscribed;
    bool gmap_config_event_subscribed;
    int timeout;
} gmap_client_t;

static gmap_client_t client = { 0 };
static amxd_dm_t* gmap_server_dm = NULL;

void gmap_client_set_mib_server_dm(amxd_dm_t* dm) {
    gmap_server_dm = dm;
}

amxd_dm_t* gmap_client_get_mib_server_dm(void) {
    return gmap_server_dm;
}

void gmap_client_init(amxb_bus_ctx_t* bus_ctx) {
    if(client.timeout != 0) {
        if(gmap_server_dm != NULL) {
            /* Called by a mib within gmap-server, silently ignore multiple init. */
            goto exit;
        } else if(amxc_llist_is_empty(&client.queries) &&
                  amxc_llist_is_empty(&client.event_handlers) &&
                  amxc_llist_is_empty(&client.config_event_handlers) &&
                  amxc_llist_is_empty(&client.device_functions)) {
            /* Multiple init by a client component. Normally, this should be
             * reported as an error. However, this situation can also arise when
             * the library is initialized as part of a test procedure.
             * In this case, no extra event handlers or data structures are still
             * alive, so we accept the different bus context as-is.
             */
            client.bus_ctx = bus_ctx;
            goto exit;
        }

        /* At this point, libgmap-client is re-initialized erroneously, either
         * by production code, or by test code that didn't clean up properly.
         * Reject the new bus context and keep the current structures, so that
         * they may be inspected with a debugger.
         */
        SAH_TRACEZ_ERROR(ME, "gmap_client is already initialized.");
        goto exit;
    }

    client.bus_ctx = bus_ctx;
    client.timeout = 20;
    client.gmap_event_subscribed = false;
    amxc_llist_init(&client.queries);
    amxc_llist_init(&client.event_handlers);
    amxc_llist_init(&client.config_event_handlers);
    amxc_llist_init(&client.device_functions);

exit:
    return;
}

amxb_bus_ctx_t* gmap_get_bus_ctx(void) {
    return client.bus_ctx;
}

amxc_llist_t* gmap_get_client_queries(void) {
    return &(client.queries);
}

amxc_llist_t* gmap_get_client_event_handlers(void) {
    return &(client.event_handlers);
}

amxc_llist_t* gmap_get_client_config_event_handlers(void) {
    return &(client.config_event_handlers);
}

int gmap_get_timeout(void) {
    return client.timeout;
}

bool gmap_event_already_subscribed(void) {
    return client.gmap_event_subscribed;
}

bool gmap_config_event_already_subscribed(void) {
    return client.gmap_config_event_subscribed;
}

void gmap_event_set_subscribed(bool subscribe) {
    client.gmap_event_subscribed = subscribe;
}

void gmap_config_event_set_subscribed(bool subscribe) {
    client.gmap_config_event_subscribed = subscribe;
}
amxc_llist_t* gmap_get_client_device_functions(void) {
    return &(client.device_functions);
}

