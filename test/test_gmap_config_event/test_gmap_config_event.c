/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>

#include "mock.h"
#include "test_gmap_config_event.h"
#include "gmap/gmap.h"

#define GMAP_EVENT_GL "global"
#define GMAP_EVENT_LO "local"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx;
static unsigned int event_rcvd_cb1;
static unsigned int event_rcvd_cb2;
static unsigned int event_rcvd_cb3;

int test_gmap_setup(UNUSED void** state) {
    printf("test setup\n");
    test_init_dummy_dm(&dm, &parser);
    bus_ctx = amxb_be_who_has("Devices");
    gmap_client_init(bus_ctx);
    event_rcvd_cb1 = 0;
    event_rcvd_cb2 = 0;
    event_rcvd_cb3 = 0;
    handle_events();
    return 0;
}

int test_gmap_teardown(UNUSED void** state) {
    test_clean_dummy_dm(&dm, &parser);
    return 0;
}

static void event_cb1(const char* module, const amxc_var_t* params) {
    printf("module:%s\n", module);
    amxc_var_dump(params, STDOUT_FILENO);
    assert_int_equal(strcmp(module, GMAP_EVENT_GL), 0);
    assert_int_equal(GETP_UINT32(params, "test_param.to"), 1);
    event_rcvd_cb1 += 1;
}

static void event_cb2(const char* module, const amxc_var_t* params) {
    printf("module:%s\n", module);
    amxc_var_dump(params, STDOUT_FILENO);
    assert_int_equal(strcmp(module, GMAP_EVENT_GL), 0);
    assert_int_equal(GETP_UINT32(params, "test_param.to"), 1);
    event_rcvd_cb2 += 1;
}

static void event_cb3(const char* module, const amxc_var_t* params) {
    printf("module:%s\n", module);
    amxc_var_dump(params, STDOUT_FILENO);
    assert_int_equal(strcmp(module, GMAP_EVENT_LO), 0);
    assert_int_equal(GETP_UINT32(params, "test_param.to"), 3);
    event_rcvd_cb3 += 1;
}


void test_gmap_config_event_handler(UNUSED void** state) {
    bool res = false;
    amxc_var_t event_data1;
    amxd_trans_t trans;

    amxc_var_init(&event_data1);
    amxc_var_set(uint32_t, &event_data1, 1);

    amxd_trans_init(&trans);

    /* invalid test 1, call back is NULL */
    res = gmap_config_event_addHandler(GMAP_EVENT_GL, NULL);
    assert_false(res);

    res = gmap_config_event_addHandler(GMAP_EVENT_GL, event_cb1);
    assert_true(res);

    handle_events();

    /*already exist, returns true */
    res = gmap_config_event_addHandler(GMAP_EVENT_GL, event_cb1);
    assert_true(res);

    res = gmap_config_event_addHandler(GMAP_EVENT_GL, event_cb2);
    assert_true(res);

    amxd_trans_select_pathf(&trans, "Devices.Config.global.");
    amxd_trans_set_param(&trans, "test_param", &event_data1);
    amxd_trans_apply(&trans, &dm);
    handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(event_rcvd_cb1, 1);
    assert_int_equal(event_rcvd_cb2, 1);
    assert_int_equal(event_rcvd_cb3, 0);

    event_rcvd_cb1 = 0;
    event_rcvd_cb2 = 0;

    amxc_var_clean(&event_data1);
}

void test_gmap_config_event_handler_extra_module(UNUSED void** state) {
    bool res = false;
    amxc_var_t event_data1;
    amxc_var_t event_data3;
    amxd_trans_t trans;

    amxc_var_init(&event_data1);
    amxc_var_set(uint32_t, &event_data1, 1);

    amxc_var_init(&event_data3);
    amxc_var_set(uint32_t, &event_data3, 3);

    amxd_trans_init(&trans);

    // Check that nothing happens when no new handler is added yet
    amxd_trans_select_pathf(&trans, "Devices.Config.local.");
    amxd_trans_set_param(&trans, "test_param", &event_data1);
    amxd_trans_apply(&trans, &dm);
    handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(event_rcvd_cb1, 0);
    assert_int_equal(event_rcvd_cb2, 0);
    assert_int_equal(event_rcvd_cb3, 0);

    // Add an extra handler with an other module value
    res = gmap_config_event_addHandler(GMAP_EVENT_LO, event_cb3);
    handle_events();
    assert_true(res);

    amxd_trans_select_pathf(&trans, "Devices.Config.local.");
    amxd_trans_set_param(&trans, "test_param", &event_data3);
    amxd_trans_apply(&trans, &dm);
    handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(event_rcvd_cb1, 0);
    assert_int_equal(event_rcvd_cb2, 0);
    assert_int_equal(event_rcvd_cb3, 1);

    amxc_var_clean(&event_data3);
}

void test_gmap_config_remove_event_handlers(UNUSED void** state) {
    bool res = false;

    res = gmap_config_event_removeHandler(GMAP_EVENT_GL, event_cb1);
    assert_true(res);
    res = gmap_config_event_removeHandler(GMAP_EVENT_GL, event_cb2);
    assert_true(res);
    res = gmap_config_event_removeHandler(GMAP_EVENT_LO, event_cb3);
    assert_true(res);
    res = gmap_config_event_removeHandler(GMAP_EVENT_GL, event_cb1);
    assert_true(res);
    handle_events();
}
