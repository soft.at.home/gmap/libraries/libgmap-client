/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_gmap_devices.h"

static amxb_bus_ctx_t dummy;

void test_gmap_findByMac__success(UNUSED void** state) {
    test_amxb_call_mockdata_t mock_data = {0};
    char* key = NULL;

    // GIVEN gmap-client

    // EXPECT bus call to happen with correct parameters, reporting key was found
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .bus_retval_string = "id-found",
        .expect_parameter1 = "mac",
        .expect_argument1_string = "11:aB:33:44:55:66"
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_findByMac
    key = gmap_devices_findByMac("11:aB:33:44:55:66");

    // THEN gmap-client returns the key
    assert_non_null(key);
    assert_string_equal("id-found", key);

    free(key);
}

void test_gmap_findByMac__not_found(UNUSED void** state) {
    test_amxb_call_mockdata_t mock_data = {0};
    char* key = NULL;

    // GIVEN gmap-client

    // EXPECT bus call to happen with correct parameters, reporting key was not found
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .bus_retval_string = "",
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_findByMac
    key = gmap_devices_findByMac("11:aB:33:44:55:66");

    // THEN gmap-client returns it's not found
    assert_null(key);
}

void test_gmap_findByMac__failure(UNUSED void** state) {
    char* key = NULL;

    // GIVEN gmap-client, and a bus that will return failure on call
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());

    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_error);

    // WHEN calling gmap_devices_findByMac
    key = gmap_devices_findByMac("11:22:33:44:55:66");

    // THEN gmap-client returns NULL
    assert_null(key);
}
