/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_gmap_devices.h"

static amxb_bus_ctx_t dummy;

void test_gmap_create_device(UNUSED void** state) {

    amxc_var_t values;
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &values, "testval", "test");

    expect_memory_count(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t), 2);
    expect_string_count(__wrap_amxb_call, object, "Devices", 2);
    expect_string_count(__wrap_amxb_call, method, "createDevice", 2);
    expect_value_count(__wrap_amxb_call, timeout, gmap_get_timeout(), 2);

    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_ok);
    assert_true(gmap_devices_createDevice_ext("TEST", "TEST_SOURCE", "tag1 tag2", false, "TEST_NAME", &values));

    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_error);
    assert_false(gmap_devices_createDevice("TEST", "TEST_SOURCE", "tag1 tag2", false, "TEST_NAME"));

    assert_false(gmap_devices_createDevice(NULL, "", NULL, false, "TEST_NAME"));
    assert_false(gmap_devices_createDevice("", "", NULL, false, "TEST_NAME"));

    amxc_var_clean(&values);
}

void test_gmap_destroy_device(UNUSED void** state) {
    expect_memory_count(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t), 2);
    expect_string_count(__wrap_amxb_call, object, "Devices", 2);
    expect_string_count(__wrap_amxb_call, method, "destroyDevice", 2);
    expect_value_count(__wrap_amxb_call, timeout, gmap_get_timeout(), 2);

    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_ok);
    assert_true(gmap_devices_destroyDevice("TEST"));

    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_error);
    assert_false(gmap_devices_destroyDevice("TEST"));
}

static void s_expect_createDeviceOrGetKey(const char* mac, bool already_exists, amxc_var_t* out_params, test_amxb_call_mockdata_t* mock_data) {
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "createDeviceOrGetKey");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    amxc_var_set_type(out_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, out_params, "key", "mykey");
    amxc_var_add_key(bool, out_params, "already_exists", already_exists);
    *mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .expect_parameter1 = "mac",
        .expect_argument1_string = mac,
        .out_params = out_params,
    };
    will_return(__wrap_amxb_call, mock_data);
}

static void s_expect_fail_createDeviceOrGetKey() {
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "createDeviceOrGetKey");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());

    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_error);

}

static void s_expect_setTag(test_amxb_call_mockdata_t* mock_data, const char* path, const char* tags) {
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, path);
    expect_string(__wrap_amxb_call, method, "setTag");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    *mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .expect_parameter1 = "tag",
        .expect_argument1_string = tags,
    };
    will_return(__wrap_amxb_call, mock_data);
}

void test_gmap_createDeviceOrGetKey__success(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    test_amxb_call_mockdata_t mock_data = {0};
    amxc_var_t out_params;
    amxc_var_init(&out_params);
    char* key = NULL;

    // GIVEN gmap-client

    // EXPECT bus call to happen with correct arguments
    s_expect_createDeviceOrGetKey("11:22:33:44:55:66", true, &out_params, &mock_data);

    // WHEN calling gmap_devices_createDeviceOrGetKey
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createDeviceOrGetKey(&key, &already_existed, "11:22:33:44:55:66",
                                           __FUNCTION__, "mac", true, "defname", &device_params);

    // THEN gmap-client claims it was successful
    assert_true(ok);
    // THEN the output arguments are passed
    assert_string_equal(key, "mykey");
    assert_true(already_existed);

    amxc_var_clean(&device_params);
    amxc_var_clean(&out_params);
    free(key);
}

void test_gmap_createDeviceOrGetKey__failure(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    char* key = NULL;

    // GIVEN gmap-client, and a bus that will return failure on call
    s_expect_fail_createDeviceOrGetKey();

    // WHEN calling gmap_devices_createDeviceOrGetKey
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createDeviceOrGetKey(&key, &already_existed, "11:22:33:44:55:66",
                                           __FUNCTION__, "mac", true, "defname", &device_params);

    // THEN gmap-client claims it was not successful
    assert_false(ok);

    amxc_var_clean(&device_params);
}

void test_gmap_createIfActive__success_exists_active(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    test_amxb_call_mockdata_t mock_data_createdev = {0};
    test_amxb_call_mockdata_t mock_data_settag = {0};
    amxc_var_t mock_out_params;
    amxc_var_init(&mock_out_params);
    char* key = NULL;

    // GIVEN gmap-client

    // EXPECT device to be created
    s_expect_createDeviceOrGetKey("11:22:33:44:55:66", true, &mock_out_params, &mock_data_createdev);
    // EXPECT device's tags to be set
    s_expect_setTag(&mock_data_settag, "Devices.Device.mykey", "mac");

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, true);

    // THEN gmap-client claims it was successful
    assert_true(ok);
    // THEN the output arguments are passed
    assert_string_equal(key, "mykey");
    assert_true(already_existed);

    amxc_var_clean(&device_params);
    amxc_var_clean(&mock_out_params);
    free(key);
}

void test_gmap_createIfActive__success_nonexists_active(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    test_amxb_call_mockdata_t mock_data_createdev = {0};
    amxc_var_t mock_out_params;
    amxc_var_init(&mock_out_params);
    char* key = NULL;

    // GIVEN gmap-client

    // EXPECT device to be created
    s_expect_createDeviceOrGetKey("11:22:33:44:55:66", false, &mock_out_params, &mock_data_createdev);

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, true);

    // THEN gmap-client claims it was successful
    assert_true(ok);
    // THEN the output arguments are passed
    assert_string_equal(key, "mykey");
    // THEN the device is reported to not already exist
    assert_false(already_existed);

    amxc_var_clean(&device_params);
    amxc_var_clean(&mock_out_params);
    free(key);
}

void test_gmap_createIfActive__success_exists_inactive(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    test_amxb_call_mockdata_t mock_data_findbymac = {0};
    test_amxb_call_mockdata_t mock_data_settag = {0};
    char* key = NULL;

    // GIVEN gmap-client

    // EXPECT device is successfully found
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data_findbymac = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .bus_retval_string = "mykey",
    };
    will_return(__wrap_amxb_call, &mock_data_findbymac);
    // EXPECT device's tags to be set
    s_expect_setTag(&mock_data_settag, "Devices.Device.mykey", "mac");

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, false);

    // THEN gmap-client claims it was successful
    assert_true(ok);
    // THEN the key/alias of the device is returned
    assert_string_equal(key, "mykey");
    // THEN the device is reported to already exist
    assert_true(already_existed);

    amxc_var_clean(&device_params);
    free(key);
}

void test_gmap_createIfActive__success_nonexists_inactive(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    test_amxb_call_mockdata_t mock_data = {0};
    char* key = "value to be able to see if this becomes NULL";

    // GIVEN gmap-client

    // EXPECT device is successfully not found
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    mock_data = (test_amxb_call_mockdata_t) {
        .c_retval = AMXB_STATUS_OK,
        .bus_retval_string = "",
    };
    will_return(__wrap_amxb_call, &mock_data);

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, false);

    // THEN gmap-client claims it was successful
    assert_true(ok);
    // THEN the key/alias of the device is set to NULL
    assert_null(key);
    // THEN the device is reported to not exist
    assert_false(already_existed);

    amxc_var_clean(&device_params);
    free(key);
}

void test_gmap_createIfActive__failure_exists_active(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    char* key = "non_null_value";

    // GIVEN gmap-client

    // EXPECT device attempted but failed to be created
    s_expect_fail_createDeviceOrGetKey();

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, true);

    // THEN gmap-client claims it failed
    assert_false(ok);
    assert_null(key);

    amxc_var_clean(&device_params);
    free(key);
}

void test_gmap_createIfActive__failure_nonexists_active(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    char* key = "non null value";

    // GIVEN gmap-client

    // EXPECT device attempted but failed to be created
    s_expect_fail_createDeviceOrGetKey();

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, true);

    // THEN gmap-client claims it failed
    assert_false(ok);
    assert_null(key);

    amxc_var_clean(&device_params);
    free(key);
}

void test_gmap_createIfActive__failure_exists_inactive(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    char* key = "non null value";

    // GIVEN gmap-client

    // EXPECT device lookup is attempted but the operation itself failed
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_error);

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, false);

    // THEN gmap-client claims it failed
    assert_false(ok);
    assert_null(key);

    amxc_var_clean(&device_params);
    free(key);
}

void test_gmap_createIfActive__failure_nonexists_inactive(UNUSED void** state) {
    bool ok = false;
    bool already_existed = false;
    amxc_var_t device_params;
    amxc_var_init(&device_params);
    char* key = "non null value";

    // GIVEN gmap-client

    // EXPECT device lookup is attempted but the operation itself failed
    expect_memory(__wrap_amxb_call, bus_ctx, &dummy, sizeof(amxb_bus_ctx_t));
    expect_string(__wrap_amxb_call, object, "Devices");
    expect_string(__wrap_amxb_call, method, "findByMac");
    expect_value(__wrap_amxb_call, timeout, gmap_get_timeout());
    will_return(__wrap_amxb_call, &test_amxb_call_mockdata_return_c_error);

    // WHEN calling gmap_devices_createIfActive
    amxc_var_set_type(&device_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &device_params, "testval", "test");
    ok = gmap_devices_createIfActive(&key, &already_existed, "11:22:33:44:55:66",
                                     __FUNCTION__, "mac", true, "defname", &device_params, false);

    // THEN gmap-client claims it failed
    assert_false(ok);
    assert_null(key);

    amxc_var_clean(&device_params);
    free(key);
}

void test_gmap_createIfActive__null_for_output_params(UNUSED void** state) {
    bool ok = false;
    test_amxb_call_mockdata_t mock_data_createdev = {0};
    test_amxb_call_mockdata_t mock_data_settag = {0};
    amxc_var_t mock_out_params;
    amxc_var_init(&mock_out_params);

    // GIVEN gmap-client

    // EXPECT device to be created
    s_expect_createDeviceOrGetKey("11:22:33:44:55:66", true, &mock_out_params, &mock_data_createdev);
    // EXPECT device's tags to be set
    s_expect_setTag(&mock_data_settag, "Devices.Device.mykey", "mac");

    // WHEN calling gmap_devices_createIfActive, but passing NULL for the output arguments
    ok = gmap_devices_createIfActive(NULL, NULL, "11:22:33:44:55:66", __FUNCTION__, "mac", true, NULL, NULL, true);

    // THEN gmap-client claims it was successful
    assert_true(ok);

    amxc_var_clean(&mock_out_params);
}

void test_gmap_createIfActive__invalid_arguments(UNUSED void** state) {
    assert_false(gmap_devices_createIfActive(NULL, NULL,
                                             NULL, // <--
                                             __FUNCTION__, "mac", true, "defname", NULL, true));
    assert_false(gmap_devices_createIfActive(NULL, NULL, "11:22:33:44:55:66",
                                             NULL, // <--
                                             "mac", true, "defname", NULL, true));

    assert_false(gmap_devices_createIfActive(NULL, NULL, "11:22:33:44:55:66", __FUNCTION__,
                                             NULL, // <--
                                             true, "defname", NULL, true));
}
