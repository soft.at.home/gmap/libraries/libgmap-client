/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <string.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>

#include "test_gmap_device.h"

#define UNUSED __attribute__((unused))


static amxb_bus_ctx_t dummy;
static amxc_var_t* amxb_call_expected_args = NULL;

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout) {
    amxc_var_t* expected_args = amxb_call_expected_args;
    int args_check = -1;
    amxb_call_expected_args = NULL;

    check_expected_ptr(bus_ctx);
    check_expected_ptr(object);
    check_expected_ptr(method);
    check_expected(timeout);

    assert_int_equal(amxc_var_type_of(args), AMXC_VAR_ID_HTABLE);
    if(expected_args != NULL) {
        function_called();
        assert_true(0 == amxc_var_compare(args, expected_args, &args_check));
        assert_int_equal(args_check, 0);
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(bool, ret, true);

    return (int) mock();
}

void test_gmap_expect_amxb_call_args(amxc_var_t* args) {
    assert_null(amxb_call_expected_args);
    assert_non_null(args);

    amxb_call_expected_args = args;
    expect_function_call(__wrap_amxb_call);
}

int test_gmap_setup(UNUSED void** state) {
    gmap_client_init(&dummy);

    return 0;
}

amxb_bus_ctx_t* test_gmap_get_bus_ctx(void) {
    return &dummy;
}

void test_gmap_device_key_object_prefix(UNUSED void** state) {
    const char* valid_key = "myTestKey";
    const char* invalid_key = "00:AA:BB:CC:DD:EE:FF";

    char* result_valid_key = gmap_create_valid_object_name(valid_key);
    char* result_invalid_key = gmap_create_valid_object_name(invalid_key);

    assert_string_equal(valid_key, result_valid_key);

    assert_int_equal(strncmp(result_invalid_key, GMAP_DEVICE_VALID_OBJECT_PREFIX, strlen(GMAP_DEVICE_VALID_OBJECT_PREFIX)), 0);


    char* reverse_result_valid = gmap_create_key_from_object_name(result_valid_key);
    char* reverse_result_invalid = gmap_create_key_from_object_name(result_invalid_key);

    assert_string_equal(reverse_result_valid, valid_key);
    assert_string_equal(reverse_result_invalid, invalid_key);


    free(result_valid_key);
    free(result_invalid_key);
    free(reverse_result_valid);
    free(reverse_result_invalid);

}


void test_internal_traverse_mode_string_conversions(UNUSED void** state) {
    // Test string to mode conversions
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_THIS_STR), gmap_traverse_this);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_DOWN_STR), gmap_traverse_down);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_UP_STR), gmap_traverse_up);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_DOWN_EXCLUSIVE_STR), gmap_traverse_down_exclusive);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_UP_EXCLUSIVE_STR), gmap_traverse_up_exclusive);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_DOWN_STR), gmap_traverse_one_down);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_UP_STR), gmap_traverse_one_up);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_DOWN_EXCLUSIVE_STR), gmap_traverse_one_down_exclusive);
    assert_int_equal(gmap_traverse_mode(GMAP_TRAVERSE_ONE_UP_EXCLUSIVE_STR), gmap_traverse_one_up_exclusive);

    // test mode to string conversions
    assert_string_equal(GMAP_TRAVERSE_THIS_STR, gmap_traverse_string(gmap_traverse_this));
    assert_string_equal(GMAP_TRAVERSE_DOWN_STR, gmap_traverse_string(gmap_traverse_down));
    assert_string_equal(GMAP_TRAVERSE_UP_STR, gmap_traverse_string(gmap_traverse_up));
    assert_string_equal(GMAP_TRAVERSE_DOWN_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_down_exclusive));
    assert_string_equal(GMAP_TRAVERSE_UP_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_up_exclusive));
    assert_string_equal(GMAP_TRAVERSE_ONE_DOWN_STR, gmap_traverse_string(gmap_traverse_one_down));
    assert_string_equal(GMAP_TRAVERSE_ONE_UP_STR, gmap_traverse_string(gmap_traverse_one_up));
    assert_string_equal(GMAP_TRAVERSE_ONE_DOWN_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_one_down_exclusive));
    assert_string_equal(GMAP_TRAVERSE_ONE_UP_EXCLUSIVE_STR, gmap_traverse_string(gmap_traverse_one_up_exclusive));

}
