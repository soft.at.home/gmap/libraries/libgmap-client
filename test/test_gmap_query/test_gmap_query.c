/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>

#include "mock.h"
#include "test_gmap_query.h"
#include "gmap/gmap.h"

typedef struct _query_cb_test_data {
    gmap_query_t* q;
    uint32_t action;
    char* key;
    amxc_var_t* device;
} query_cb_test_data_t;

query_cb_test_data_t query_cb_test_data;

static amxd_dm_t dm;
static amxo_parser_t parser;
static unsigned int cb_count = 0;
static amxb_bus_ctx_t* bus_ctx;

int test_gmap_setup(UNUSED void** state) {
    printf("test setup\n");
    test_init_dummy_dm(&dm, &parser);
    bus_ctx = amxb_be_who_has("Devices");
    gmap_client_init(bus_ctx);

    handle_events();

    return 0;
}

int test_gmap_teardown(UNUSED void** state) {
    test_clean_dummy_dm(&dm, &parser);

    return 0;
}

static void set_query_cb_test_data(gmap_query_t* q,
                                   uint32_t action,
                                   const char* key,
                                   amxc_var_t* device) {
    query_cb_test_data.q = q;
    query_cb_test_data.action = action;
    query_cb_test_data.key = strdup(key);
    query_cb_test_data.device = device;
}

static void clean_query_cb_test_data(void) {
    free(query_cb_test_data.key);
    if(query_cb_test_data.device) {
        amxc_var_delete(&query_cb_test_data.device);
        query_cb_test_data.device = NULL;
    }
    query_cb_test_data = (query_cb_test_data_t) { 0 };
}

static void query_cb(gmap_query_t* query, const char* key, amxc_var_t* device, gmap_query_action_t action) {
    cb_count++;
    assert_non_null(query);
    printf("query_cb called\n");
    query_cb_test_data.q = query;
    assert_int_equal(query_cb_test_data.action, action);
    assert_string_equal(query_cb_test_data.key, key);
    if(action != gmap_query_expression_stop_matching) {
        assert_non_null(device);
        const char* received_key = GET_CHAR(device, "Key");
        assert_non_null(received_key);
        assert_string_equal(received_key, query_cb_test_data.key);
    }
}

static void test_gmap_verify_query_call(gmap_query_t* q,
                                        bool verify_cb) {
    /*check we can find the dm query object */
    assert_non_null(q);
    amxd_object_t* query_tmpl_obj = amxd_dm_findf(&dm, "Devices.Query.");
    assert_non_null(query_tmpl_obj);
    amxd_object_t* query_obj = amxd_object_get_instance(query_tmpl_obj, NULL, q->index);
    assert_non_null(query_obj);

    if(verify_cb) {
        amxc_var_t sig_data;
        amxc_var_t* data = NULL;
        amxc_var_t* device_data = NULL;
        amxc_var_init(&sig_data);
        amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
        data = amxc_var_add_new_key(&sig_data, "Result");
        amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(uint32_t, data, "Id", q->id);
        amxc_var_add_key(uint32_t, data, "Index", q->index);
        amxc_var_add_key(uint32_t, data, "Action", query_cb_test_data.action);
        amxc_var_add_key(cstring_t, data, "Key", query_cb_test_data.key);
        if(query_cb_test_data.action != gmap_query_expression_stop_matching) {
            if(query_cb_test_data.device) {
                printf("add device to query event\n");
                device_data = amxc_var_add_new_key(data, "Device");
                amxc_var_copy(device_data, query_cb_test_data.device);
            }
        }
        amxd_object_emit_signal(query_obj, "gmap_query", &sig_data);
        handle_events();
        amxc_var_clean(&sig_data);
    }
}

static amxc_var_t* test_gmap_create_dummy_device_var(char* key) {
    amxc_var_t* device = NULL;
    amxc_var_new(&device);
    amxc_var_set_type(device, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, device, "Key", key);
    return device;
}

static void test_gmap_check_matching_devices(amxc_var_t* matching_devices) {
    const char* key = GETI_CHAR(matching_devices, 0);
    amxc_var_dump(matching_devices, STDOUT_FILENO);

    assert_non_null(key);
    assert_string_equal(key, query_cb_test_data.key);
}

static void _assert_gmap_query_flags(gmap_query_flags_t flags, const char* expected, const char* file, int line) {
    amxc_string_t* str_value = gmap_query_flags_string(flags);
    _assert_true(str_value != NULL, "gmap_query_flags_string(flags) != NULL", file, line);
    _assert_string_equal(amxc_string_get(str_value, 0), expected, file, line);
    amxc_string_delete(&str_value);
}

#define assert_gmap_query_flags(a, b) _assert_gmap_query_flags(a, b, __FILE__, __LINE__)

void test_gmap_query_flags(UNUSED void** state) {
    assert_int_equal(gmap_query_flags_from_cstring(NULL),
                     0);
    assert_int_equal(gmap_query_flags_from_cstring(""),
                     0);
    assert_int_equal(gmap_query_flags_from_cstring("not a valid flag"),
                     0);
    assert_int_equal(gmap_query_flags_from_cstring("not | valid flags"),
                     0);
    assert_int_equal(gmap_query_flags_from_cstring(GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR),
                     GMAP_QUERY_IGNORE_DEVICE_UPDATED);
    assert_int_equal(gmap_query_flags_from_cstring(GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR "|"),
                     GMAP_QUERY_IGNORE_DEVICE_UPDATED);
    assert_int_equal(gmap_query_flags_from_cstring(GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR "|other"),
                     GMAP_QUERY_IGNORE_DEVICE_UPDATED);
    assert_int_equal(gmap_query_flags_from_cstring("other|" GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR "|somethingelse"),
                     GMAP_QUERY_IGNORE_DEVICE_UPDATED);
    assert_int_equal(gmap_query_flags_from_cstring("with spaces|  " GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR "  |somethingelse"),
                     GMAP_QUERY_IGNORE_DEVICE_UPDATED);

    assert_gmap_query_flags(0, "");
    assert_gmap_query_flags(GMAP_QUERY_IGNORE_DEVICE_UPDATED, GMAP_QUERY_IGNORE_DEVICE_UPDATED_STR);
    assert_gmap_query_flags(~GMAP_QUERY_IGNORE_DEVICE_UPDATED, "");
}

void test_gmap_open_query(UNUSED void** state) {

    bool res;
    unsigned int count_cb = cb_count;
    uint32_t index = 0;
    uint32_t id = 0;
    gmap_query_flags_t flags = 0;
    const char* name = NULL;
    const char* expression = NULL;
    gmap_query_t* query1 = NULL;
    gmap_query_t* query2 = NULL;
    amxc_var_t* matching_devices;
    amxc_var_t* device1;
    amxc_var_t* device2;

    /* valid queries */
    assert_true(gmap_query_open_ext_2(&query1, "self", "query1", 0, query_cb, ""));

    assert_non_null(query1);

    assert_true(gmap_query_index(query1, &index));
    assert_int_equal(index, query1->index);

    assert_true(gmap_query_id(query1, &id));
    assert_int_equal(id, query1->id);

    assert_true(gmap_query_flags(query1, &flags));
    assert_int_equal(flags, query1->flags);

    name = gmap_query_name(query1);
    assert_string_equal(name, "query1");

    expression = gmap_query_expression(query1);
    assert_string_equal(expression, "self");

    assert_int_equal(count_cb, cb_count);
    test_gmap_verify_query_call(query1, false);

    printf("create device 1\n");
    /* create matching device after open query */
    res = gmap_devices_createDevice("device1", "", "self", false, "");
    assert_true(res);

    device1 = test_gmap_create_dummy_device_var("device1");
    assert_non_null(device1);
    set_query_cb_test_data(query1, gmap_query_expression_start_matching, "device1", device1);
    test_gmap_verify_query_call(query1, true);
    assert_int_equal(++count_cb, cb_count);

    matching_devices = gmap_query_matchingDevices(query1);
    test_gmap_check_matching_devices(matching_devices);
    amxc_var_delete(&matching_devices);

    printf("create device 2\n");
    /* create matching device before open query */
    res = gmap_devices_createDevice("device2", "", "hgw", false, "");
    assert_true(res);

    device2 = test_gmap_create_dummy_device_var("device2");
    assert_non_null(device2);
    clean_query_cb_test_data();
    set_query_cb_test_data(NULL, gmap_query_expression_start_matching, "device2", device2);
    assert_true(gmap_query_open_ext_2(&query2, "hgw", "query2", GMAP_QUERY_IGNORE_DEVICE_UPDATED | 0xf000, query_cb, "device2"));
    test_gmap_verify_query_call(query2, false);
    assert_ptr_equal(query2, query_cb_test_data.q);
    assert_int_equal(++count_cb, cb_count);
    assert_int_equal(query2->flags, GMAP_QUERY_IGNORE_DEVICE_UPDATED);

    /* destroy device 1 */

    clean_query_cb_test_data();
    set_query_cb_test_data(query1, gmap_query_expression_stop_matching, "device1", NULL);
    res = gmap_devices_destroyDevice("device1");
    assert_true(res);
    test_gmap_verify_query_call(query1, true);
    assert_int_equal(++count_cb, cb_count);

    /* destroy device 2 */
    clean_query_cb_test_data();
    set_query_cb_test_data(query2, gmap_query_expression_stop_matching, "device2", NULL);
    res = gmap_devices_destroyDevice("device2");
    assert_true(res);
    test_gmap_verify_query_call(query2, true);
    assert_int_equal(++count_cb, cb_count);

    clean_query_cb_test_data();
    gmap_query_close(query1);
    gmap_query_close(query2);

    handle_events();
}

static void query_bad_cb(UNUSED gmap_query_t* query, UNUSED const char* key,
                         UNUSED amxc_var_t* device, UNUSED gmap_query_action_t action) {
    /* We expect this function to never be called. */
    function_called();
}

void test_gmap_open_query_invalid(UNUSED void** state) {
    gmap_query_t* query = NULL;

    assert_null(gmap_query_open_ext(NULL, "query1", query_bad_cb, "userdata"));
    assert_null(gmap_query_open_ext("self", "", query_bad_cb, "userdata"));
    assert_null(gmap_query_open_ext("self", "query1", NULL, "userdata"));

    assert_false(gmap_query_open_ext_2(&query, NULL, "query1", 0, query_bad_cb, "userdata"));
    assert_null(query);
    assert_false(gmap_query_open_ext_2(&query, "self", "", 0, query_bad_cb, "userdata"));
    assert_null(query);
    assert_false(gmap_query_open_ext_2(&query, "self", "query1", 0, NULL, "userdata"));
    assert_null(query);
    assert_false(gmap_query_open_ext_2(NULL, "self", "query1", 0, query_bad_cb, "userdata"));
    assert_null(query);
}

static void query_closing_cb(gmap_query_t* query, const char* key, amxc_var_t* device, gmap_query_action_t action) {
    assert_non_null(query);
    uint32_t* cb_count = (uint32_t*) query->data;
    printf("query_cb called\n");
    if(cb_count != NULL) {
        (*cb_count)++;
    }

    query_cb_test_data.q = query;
    assert_int_equal(query_cb_test_data.action, action);
    assert_string_equal(query_cb_test_data.key, key);
    if(action != gmap_query_expression_stop_matching) {
        assert_non_null(device);
        const char* received_key = GET_CHAR(device, "Key");
        assert_non_null(received_key);
        assert_string_equal(received_key, query_cb_test_data.key);
    }
    assert_non_null(amxd_dm_findf(&dm, "Devices.Query.query."));
    gmap_query_close(query);
    assert_null(amxd_dm_findf(&dm, "Devices.Query.query."));
}

void test_gmap_close_query_from_callback_one_matching(UNUSED void** state) {
    bool res;
    gmap_query_t* query = NULL;
    uint32_t count_cb = 0;
    amxc_var_t* device1 = NULL;

    clean_query_cb_test_data();
    res = gmap_devices_createDevice("device1", "", "self", false, "");
    assert_true(res);

    device1 = test_gmap_create_dummy_device_var("device1");
    set_query_cb_test_data(NULL, gmap_query_expression_start_matching, "device1", device1);
    assert_null(amxd_dm_findf(&dm, "Devices.Query.query."));
    query = gmap_query_open_ext("self", "query", query_closing_cb, &count_cb);
    assert_int_equal(count_cb, 1);

    res = gmap_devices_destroyDevice("device1");
    assert_true(res);

    clean_query_cb_test_data();
    gmap_query_close(query);
    handle_events();
}

void test_gmap_close_query_from_callback_two_matching(UNUSED void** state) {
    bool res;
    gmap_query_t* query = NULL;
    uint32_t count_cb = 0;
    amxc_var_t* device1 = NULL;

    clean_query_cb_test_data();
    res = gmap_devices_createDevice("device1", "", "self", false, "");
    assert_true(res);
    res = gmap_devices_createDevice("device2", "", "self", false, "");
    assert_true(res);

    device1 = test_gmap_create_dummy_device_var("device1");
    set_query_cb_test_data(NULL, gmap_query_expression_start_matching, "device1", device1);
    assert_true(gmap_query_open_ext_2(&query, "self", "query", 0, query_closing_cb, &count_cb));
    assert_null(query);
    assert_int_equal(count_cb, 1);

    res = gmap_devices_destroyDevice("device1");
    assert_true(res);
    res = gmap_devices_destroyDevice("device2");
    assert_true(res);

    clean_query_cb_test_data();
    gmap_query_close(query);
    handle_events();
}

void test_gmap_close_query_from_callback_event_matching(UNUSED void** state) {
    bool res;
    gmap_query_t* query = NULL;
    uint32_t count_cb = 0;
    amxc_var_t* device1 = NULL;

    clean_query_cb_test_data();

    query = gmap_query_open_ext("self", "query", query_closing_cb, &count_cb);
    assert_non_null(query);
    assert_int_equal(count_cb, 0);
    assert_non_null(amxd_dm_findf(&dm, "Devices.Query.query."));

    res = gmap_devices_createDevice("device1", "", "self", false, "");
    assert_true(res);
    device1 = test_gmap_create_dummy_device_var("device1");
    set_query_cb_test_data(NULL, gmap_query_expression_start_matching, "device1", device1);

    test_gmap_verify_query_call(query, true);
    assert_int_equal(count_cb, 1);
    assert_null(amxd_dm_findf(&dm, "Devices.Query.query."));

    res = gmap_devices_destroyDevice("device1");
    assert_true(res);

    clean_query_cb_test_data();
    handle_events();
}

void test_gmap_close_query_from_callback_one_matching_with_other_query(UNUSED void** state) {
    bool res;
    gmap_query_t* existing_query = NULL;
    gmap_query_t* query = NULL;
    uint32_t existing_count_cb = cb_count;
    uint32_t count_cb = 0;
    amxc_var_t* device1 = NULL;

    clean_query_cb_test_data();
    res = gmap_devices_createDevice("device1", "", "self", false, "");
    assert_true(res);

    device1 = test_gmap_create_dummy_device_var("device1");

    set_query_cb_test_data(NULL, gmap_query_expression_start_matching, "device1", device1);
    assert_null(amxd_dm_findf(&dm, "Devices.Query.existing_query."));
    existing_query = gmap_query_open_ext("self", "existing_query", query_cb, &existing_count_cb);
    assert_non_null(amxd_dm_findf(&dm, "Devices.Query.existing_query."));
    assert_int_equal(++existing_count_cb, cb_count);

    assert_null(amxd_dm_findf(&dm, "Devices.Query.query."));
    query = gmap_query_open_ext("self", "query", query_closing_cb, &count_cb);
    assert_int_equal(count_cb, 1);

    assert_non_null(amxd_dm_findf(&dm, "Devices.Query.existing_query."));
    query_cb_test_data.action = gmap_query_device_updated;
    test_gmap_verify_query_call(existing_query, true);
    assert_int_equal(++existing_count_cb, cb_count);

    res = gmap_devices_destroyDevice("device1");
    assert_true(res);

    clean_query_cb_test_data();
    gmap_query_close(query);
    gmap_query_close(existing_query);
    handle_events();
}
